$(function() {

    var ddBtn = function() {
        $(".js-dd").stop().slideToggle("slow");
        $("html").stop().toggleClass("sb-active");
    };

    $(".js-dd-btn").on('tap click', function() {
        ddBtn();
    });

    $(".js-soc-btn").on('tap click', function() {
        $(this).siblings(".js-soc-dd").stop().slideToggle("slow");
        $(this).toggleClass("active");
    });


    var lefMenu = function() {
        if ($(window).width() >= 1550) {

            // article__nav

            if ($("div").is(".js-article__nav_fixed")) {
                var topMenuHeight = $('.main-header__inner').height();
                var topOffset = $('.js-article__nav_fixed').offset().top - topMenuHeight;
            }

            $(window).scroll(function() {
                if ($("div").is(".js-article__nav_fixed")) {
                    var top = $(document).scrollTop();
                    if (top < topOffset) $(".js-article__nav_fixed").removeClass("menu-fix");
                    else $(".js-article__nav_fixed").addClass("menu-fix");
                }
            });


            // userbackend-page__menu

            if ($("div").is(".js__nav_fixed")) {
                var topMenuHeight2 = $('.main-header__inner').height();
                var topOffset2 = $('.js__nav_fixed').offset().top - topMenuHeight2;
            }

            $(window).scroll(function() {
                if ($("div").is(".js__nav_fixed")) {
                    var top = $(document).scrollTop();
                    if (top < topOffset2) $(".js__nav_fixed").removeClass("menu-fix");
                    else $(".js__nav_fixed").addClass("menu-fix");
                }
            });

        }
    }

    $(window).on('resize load', lefMenu);


    // search panel
    var searchPanelIsShow = false,
        searchPanel = $('.header__search-form .search-form__input-group-input'),
        searchInput = $('#search-input'),
        searchForm = $('#mini-search-form');

    $('.search-form__input-group-btn')
        .click(function() {

            if (!searchPanelIsShow) {
                searchPanelIsShow = true;
                searchPanel.css('width', '200px');
            } else {
                if (!searchInput.val()) {
                    searchPanelIsShow = false;
                    searchPanel.css('width', '0');
                } else {
                    searchForm.submit();
                }
            }
        });


    // anchor
    $(document).ready(function() {
        $('a[href*=#]').bind("click", function(e) {
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top
            }, 1000);
            e.preventDefault();
        });
        return false;
    });


    $('.prompt_selection .prompt_selection__item').click(function() {
        $('.active').removeClass('active');
        $(this).addClass('active').find('input').prop('checked', true);

        return false;
    });

    // animation
    var $window = $(window),
        win_height_padded = $window.height() * 1.1,
        isTouch = Modernizr.touch;
    if (isTouch) {
        $('.revealOnScroll').addClass('animated');
    }
    $window.on('scroll', revealOnScroll);

    function revealOnScroll() {
        var scrolled = $window.scrollTop(),
            win_height_padded = $window.height() * 1.1;
        $('.revealOnScroll:not(.animated)').each(function() {
            var $this = $(this),
                offsetTop = $this.offset().top;
            if (scrolled + win_height_padded > offsetTop) {
                if ($this.data('timeout')) {
                    window.setTimeout(function() {
                        $this.addClass('animated ' + $this.data('animation'));
                    }, parseInt($this.data('timeout'), 10));
                } else {
                    $this.addClass('animated ' + $this.data('animation'));
                }
            }
        });
        // $('.revealOnScroll.animated').each(function(index) {
        //     var $this = $(this),
        //         offsetTop = $this.offset().top;
        //     if (scrolled + win_height_padded < offsetTop) {
        //         $(this).removeClass('animated slideInLeft slideInRight fadeIn bounceInDown bounceInLeft bounceInRight zoomInLeft zoomInUp zoomInRight flipInX fadeInUp');
        //     }
        // });
    }
    revealOnScroll();


    // Carousels
    $('.owl-carousel1').owlCarousel({
        // animateOut: 'slideOutDown',
        // animateIn: 'flipInX',
        // stagePadding: 15,
        // loop: true,
        margin: 5,
        nav: true,
        items: 1,
        // touchDrag: false,
        // mouseDrag: false,
        // autoplay: true,
        // autoplayHoverPause: true,
        // autoplayTimeout: 3000,
        loop: true,
        dots: false,
        smartSpeed: 450,
        // center: true,
        navText: [
            '<span class="btn btn-square"><i class="fa fa-caret-left"></i></span>',
            '<span class="btn btn-square"><i class="fa fa-caret-right"></i></span>'
        ]
    });

    // Carousels
    $('.owl-carousel-dvsion').owlCarousel({
        margin: 5,
        nav: false,
        items: 1,
        smartSpeed: 450,
        dotData: true,
        dotsData: true
    });

    // game Carousels
    $('.owl-carousel-league-game').owlCarousel({
        margin: 5,
        nav: true,
        items: 1,
        smartSpeed: 650,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        loop: true
    });


    // Placeholder fix for IE
    $('.lt-ie10 [placeholder]').focus(function() {
        var i = $(this);
        if (i.val() == i.attr('placeholder')) {
            i.val('').removeClass('placeholder');
            if (i.hasClass('password')) {
                i.removeClass('password');
                this.type = 'password';
            }
        }
    }).blur(function() {
        var i = $(this);
        if (i.val() == '' || i.val() == i.attr('placeholder')) {
            if (this.type == 'password') {
                i.addClass('password');
                this.type = 'text';
            }
            i.addClass('placeholder').val(i.attr('placeholder'));
        }
    }).blur().parents('form').submit(function() {
        //if($(this).validationEngine('validate')) { // If using validationEngine
        $(this).find('[placeholder]').each(function() {
                var i = $(this);
                if (i.val() == i.attr('placeholder'))
                    i.val('');
                i.removeClass('placeholder');

            })
            //}
    });


});
